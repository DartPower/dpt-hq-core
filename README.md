# DPT.HQ

## Core component

Funny cooperative+deathmatch entertaining modification for DOOM II. Created for ZDoom32 port.

Also have Delta-Touch compatibility

## How to compile (Windows)

1. Download or clone repo
2. Click on build.bat
3. Wait
4. Drag & Drop pk7 to zandronum.exe
5. Play

## How to compile (Linux)

1. sudo apt-get install git
2. git clone -b zd32funcs git@github.com:dpteam/dpt-hq-core.git dpt-hq-core
3. sh build.sh
4. Wait
5. "./zdoom32 dpt-hq-core-current.pk7"
6. Play

## Current important info

CREDITS Currently Work in Progress, if im forgot some, please say it in issues. [Issue #7]

Also info for paranoics:

7z console archiver here from official 32-bit release 18.01

You can check 3 hash types (CRC32, MD5 & SHA1) in "7z.*.hash" files,

open with your archiver the exe of installer and extract files,

and compare hashes with official installed files. (Or extracted from original installer)

Link to original installer: https://www.7-zip.org/a/7z1801.exe

## Contribution info

If you want to help the project, you can check the folder named "docs".


Also you can donate: (Currently only russian services, but you can donate with $ or €)

http://www.donationalerts.ru/r/dartpower

Or here:

https://qiwi.me/dartpower

## Social & Statistics

Development status:  
[![Github All Releases](https://img.shields.io/github/downloads/dpteam/dpt-hq-core/total.svg?style=for-the-badge)](https://github.com/dpteam/dpt-hq-core/releases)
[![GitHub commit activity the past week, 4 weeks, year](https://img.shields.io/github/commit-activity/y/dpteam/dpt-hq-core.svg?style=for-the-badge)](https://github.com/dpteam/dpt-hq-core/commits/zd32funcs)
[![GitHub last commit](https://img.shields.io/github/last-commit/dpteam/dpt-hq-core.svg?style=for-the-badge)](https://github.com/dpteam/dpt-hq-core/commits/zd32funcs)
[![GitHub repo size in bytes](https://img.shields.io/github/repo-size/dpteam/dpt-hq-core.svg?style=for-the-badge)](https://github.com/dpteam/dpt-hq-core)

Social status:  
[![Discord](https://img.shields.io/discord/188620980426375168.svg?style=for-the-badge)](http://discord.me/dpt)
[![GitHub issues](https://img.shields.io/github/issues/dpteam/dpt-hq-core.svg?style=for-the-badge)](https://github.com/dpteam/dpt-hq-core/issues)
[![GitHub pull requests](https://img.shields.io/github/issues-pr/dpteam/dpt-hq-core.svg?style=for-the-badge)](https://github.com/dpteam/dpt-hq-core/pulls)
[![GitHub contributors](https://img.shields.io/github/contributors/dpteam/dpt-hq-core.svg?style=for-the-badge)](https://github.com/dpteam/dpt-hq-core/graphs/contributors)  
[![GitHub forks](https://img.shields.io/github/forks/dpteam/dpt-hq-cor.svg?style=social&label=Fork&style=for-the-badge)](https://github.com/dpteam/dpt-hq-core/network/members)
[![GitHub stars](https://img.shields.io/github/stars/dpteam/dpt-hq-core.svg?style=social&label=Stars&style=for-the-badge)](https://github.com/dpteam/dpt-hq-core/stargazers)
[![GitHub watchers](https://img.shields.io/github/watchers/dpteam/dpt-hq-core.svg?style=social&label=Watch&style=for-the-badge)](https://github.com/dpteam/dpt-hq-core/watchers) 
[![GitHub followers](https://img.shields.io/github/followers/dpteam.svg?style=social&label=Follow&style=for-the-badge)](https://github.com/dpteam)
